package edu.towson.cosc435.naurot.tipcalculatoractivity

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import kotlin.math.roundToInt


//private var calculate_tip_btn: Button? = null
//private var radio_group: RadioGroup? = null

class MainActivity : AppCompatActivity() {

    // code goes here
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO - 1. attach a click listener to the convert button and call handleClick
        val calculate_tip_btn: Button = findViewById(R.id.calculate_tip_btn)
        calculate_tip_btn.setOnClickListener { handleClick() }

        val radio_group: RadioGroup = findViewById(R.id.radio_group)
        radio_group.setOnCheckedChangeListener { _: RadioGroup, _: Int -> handleClick() }

        val check_amount_et: EditText = findViewById(R.id.check_amount_et)
        check_amount_et.addTextChangedListener { handleClick() }
    }

    private fun handleClick() {
        // TODO - 2. Get the text from the edit text (.editableText.ToString())
        val check_amount_et = findViewById<EditText>(R.id.check_amount_et)
        val checkAmountInputString = check_amount_et.editableText.toString()
        if (checkAmountInputString.isEmpty()) {
            val result_tv = findViewById<TextView>(R.id.result_tv)
            result_tv.text = "Check Amount? "
        } else if (!checkAmountInputString.equals(".")) {
            // TODO - 3. Get the selected convert type from the selected RadioButton
            val radio_group = findViewById<RadioGroup>(R.id.radio_group)
            val selectedId = radio_group.checkedRadioButtonId
            val tipPercent = when (selectedId) {
                R.id.tip_10_btn -> .1
                R.id.tip_20_btn -> .2
                R.id.tip_30_btn -> .3
                else -> throw Exception("Not expected")
            }

            // TODO - 4. Convert the string input to a double (.toDouble())
            val checkAmountInputDouble = checkAmountInputString.toDouble()

            // TODO - 6. Call the convertTemp function to convert the value
            val tipAmount = calculateTip(checkAmountInputDouble, tipPercent)
            val checkTotal = checkAmountInputDouble + tipAmount

            // TODO - 7. Set the result text based on the result (or an error message)
            val result_tv = findViewById<TextView>(R.id.result_tv)
            result_tv.text = "Your calculated tip is $${currencyToString(tipAmount)}\n         and your total is $${currencyToString(checkTotal)}"
        }
    }
}

private fun currencyToString(amount: Double): String {
    val penniesInt = (amount * 100).roundToInt(); //convert to pennies and round off
    var penniesString = penniesInt.toString()
    when (penniesString.length) {
        0 -> penniesString = "000"
        1 -> penniesString = "00" + penniesString
        2 -> penniesString = "0" + penniesString
    }
    val dollarAmount = penniesString.substring(0, penniesString.length - 2);
    val penniesAmount = penniesString.substring(penniesString.length - 2, penniesString.length)
    return dollarAmount + "." + penniesAmount
}

private fun calculateTip(checkAmountInputDouble: Double, tipPercent: Double): Double {
    return checkAmountInputDouble * tipPercent
}
